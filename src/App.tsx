import React from 'react';
import {Navbar} from "components/shared/navbar-components/navbar/navbar";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import routes from '../src/routes';

import './App.css';

function App() {
  const routesComponents = routes.map((route) => (
    <Route {...route} key={route.path} />
  ));
  return (
    <Router>
      <Navbar />
      <div className="app-container">
        <Switch>{routesComponents}</Switch>
      </div>
    </Router>
  );
}

export default App;
