import React, {FC} from 'react';
import {
  withRouter
} from "react-router-dom";
import {RouteComponentProps} from "react-router";

type PathParamsType = {
  id?: string,
}
type Props = RouteComponentProps<PathParamsType>;

// @ts-ignore
const DoctorInfo:FC<Props> = withRouter((props) => {

  return(
    <>
      <h1>Doctor Info</h1>
      <h2>{props.match.params.id}</h2>
    </>
  )
})

export {DoctorInfo};