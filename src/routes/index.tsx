import React from 'react';
import { DoctorInfo } from 'pages/DoctorInfo/DoctorInfo';


interface Route {
  name: string;
  path: string;
  component: React.FC<any>;
  exact?: boolean;
}

const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: () => <>Home</>,
    exact: true,
  },
  {
    name: 'findAHospital',
    path: '/findAHospital',
    component: () => <>Find a hospital</>,
    exact: true,
  },
  {
    name: 'treatments',
    path: '/treatments',
    component: () => <>Treatments</>,
    exact: true,
  },
  {
    name: 'medicalTrip',
    path: '/medicalTrip',
    component: () => <>Medical Trip</>,
    exact: true,
  },
  {
    name: 'information',
    path: '/information',
    component: () => <>How it works</>,
    exact: true,
  },
  {
    name: 'doctorInfo',
    path: '/doctors/:id',
    component: DoctorInfo,
  },
  {
    name: 'page404',
    path: '**',
    component: () => <>Page404</>,
  },
];

const routesMap: { [name: string]: string } = routes.reduce(
  (prevRoutes, currentRoute) => ({
    ...prevRoutes,
    [`${currentRoute.name}`]: currentRoute.path,
  }),
  {},
);

export default routes;
export { routesMap };
