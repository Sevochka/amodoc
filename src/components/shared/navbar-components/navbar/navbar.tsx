import React, {FC, useState} from 'react';
import logo from 'assets/logo.svg';
import { NavLink } from 'react-router-dom';
import {routesMap} from "routes";

import "./navbar.css";

const navLinks = [
  {
    to: routesMap.findAHospital,
    mainText: 'Find a hospital',
    secondaryText: 'Find a clinics worldwide'
  },
  {
    to: routesMap.treatments,
    mainText: 'Treatments',
    secondaryText: 'Choose medical area'
  },
  {
    to: routesMap.medicalTrip,
    mainText: 'Medical trip',
    secondaryText: 'Flight, hotel, visa, etc.'
  },
  {
    to: routesMap.information,
    mainText: 'Information',
    secondaryText: 'About Hospital service'
  }
]
const Navbar:FC = () => {
  const [openMenu, setOpenMenu] = useState(false);
  const toggleActiveView = () => {
    setOpenMenu(prevState => !prevState);
  }

  return (
    <nav className="navbar">
      <NavLink
        exact
        to={routesMap.home}
        key="How"
        className="navbar__brand"
        activeClassName="nav-menu-item-selected"
      >
        <img src={logo} alt="Logo"/>
        <span className="navbar__brand-title">Amo<span className="colored">doc</span>.com</span>
      </NavLink>


      <button className="navbar__toggle-button" onClick={toggleActiveView}>
        <span className="bar"/>
        <span className="bar"/>
        <span className="bar"/>
      </button>

      <div className={`navbar__links ${openMenu && 'active'}`}>
        {navLinks.map(({mainText, secondaryText, to}) =>
          <NavLink
            exact
            to={to}
            key={to}
            className="navbar__link"
            activeClassName="navbar__link-selected"
          >
            <span className="main-text">{mainText}</span>
            <span className="secondary-text">{secondaryText}</span>
          </NavLink>
        )}

      </div>

      <div className={`navbar__buttons ${openMenu && 'active'}`}>
        <button className="navbar__signUp">Sign Up</button>
        <button className="navbar__logIn">Log In</button>
      </div>
    </nav>
  )
}

export {Navbar};